So I've done some research on Ambient and their ECS Design, sharing it here cause maybe you're interested as well :D

Let me just start by saying that there is little to no documentation available so everything has been read from src / guesswork what it should do. Also, I'm sorry if this sounds like I'm roasting them a little :D

This is their main entity:
```rust
pub struct Entity(pub(crate) HashMap<u32, host::ComponentTypeResult>);
```
https://github.com/AmbientRun/Ambient/blob/main/guest/rust/api/src/internal/component/mod.rs#L60

No special magic going on, literally just a HashMap with your typical dynamic memory allocation. For each component you register a new id is generated, so you can add your component to any entity you like. The value ComponentTypeResult is just a giant enum of all possible types: (), bool, f32, i32, Vec2..4, Mat4, String to name a few as well as List / Option of any of these types. The largest one being Mat4 with 16 f32's making each entry 4b key + 1b enum + 16 * 4b per f32 = 69b, +padding to 8 bytes = 72b large, even if all your doing is storing a simple f32.

New Components can be registered using their `ambient.toml` (or their internal macros) which will then generate rust boilerplate code for you, and look like this:
```toml
[components]
cell = { type = "I32", name = "Cell", description = "The ID of the cell this player is in", attributes = [
    "Debuggable",
] }
```
https://github.com/AmbientRun/Ambient/blob/main/guest/rust/examples/tictactoe/ambient.toml#L6

Now first back to the HashMap, where does the key come from? Somewhere (haven't found where) these keys are getting registered into the `components` Vec and `component_paths` HashMap of the global ComponentRegistry:
```rust
static COMPONENT_REGISTRY: Lazy<RwLock<ComponentRegistry>> = Lazy::new(|| RwLock::new(ComponentRegistry::default()));
pub struct ComponentRegistry {
    pub(crate) components: Vec<RegistryComponent>,
    pub component_paths: HashMap<String, u32>,
    pub next_index: u32, // WTF this is unused??? (my addition)
}
```
https://github.com/AmbientRun/Ambient/blob/main/crates/ecs/src/component_registry.rs#L107

And here is the registration code:
```rust
let index = self.components.len().try_into().expect("Maximum component count exceeded");
slot.insert(index);
```
https://github.com/AmbientRun/Ambient/blob/main/crates/ecs/src/component_registry.rs#L140

In other words the index which is used to address the HashMap of Entity is just an incrementing integer, which I'm not actually sure if that's a bad thing in regard to hash collisions on decent HashMap implementations. But one thing I do wonder about is if it's really necessary to use the default SipHash Hasher Rust uses to be HashDos resistant or if a faster simpler hasher would be enough here, maybe even one optimized for integers? Or it may even be beneficial to replace the HashMap with a simple array for values which are present for like 99% of the objects, like EntityId, position, rotation and the like, and those which are only sometimes present get to live in a HashMap?

I should however add this little asterix here: There also seems to be some sort of grouping mechanism of similar types using this Archetype. However, I haven't figured out how it works or how it connects to the "normal" ECS I talked about above.
https://github.com/AmbientRun/Ambient/blob/main/crates/ecs/src/archetype.rs



Ok let's look at the actual standout feature of GPU ECS which we were all so interested in. First of all the project uses WebGPU as it's graphics API, and it's unique WGSL rust-like shading language. The main way the GPU's ECS seems to be updated is by pasting whatever update code into a WGSL shader's main function.
https://github.com/AmbientRun/Ambient/blob/main/crates/core/src/gpu_ecs/update.wgsl

Interestingly this pasting and compiling of the WGSL src code seems to happen during runtime, not during compile time, not sure if this project is at fault or if this is just a limitation of WebGPU itself.
https://github.com/AmbientRun/Ambient/blob/main/crates/core/tests/gpu_ecs.rs#L163

Here is the code for syncing the ECS up to the GPU, and note how it makes use of archetype to grab the ECS data. It will also only sync certain components of an entity up to the GPU which have been previously registered with a mapping from which CPU component it should pull its data from.
```rust
let gpu_world = world.resource(gpu_world()).lock();
let gpu = world.resource(gpu()).clone();
for arch in self.source_archetypes.iter_archetypes(world) {
    if let Some((gpu_buff, offset, layout_version)) = gpu_world.get_buffer(self.format, self.destination_component, arch.id) {
        if self.changed.changed(arch, self.source_component, layout_version) {
            let buf = arch.get_component_buffer(self.source_component).unwrap();
            gpu.queue.write_buffer(gpu_buff, offset, bytemuck::cast_slice(&buf.data));
        }
    }
}
```
https://github.com/AmbientRun/Ambient/blob/main/crates/core/src/gpu_ecs/sync.rs#L57

Note that I've only talked about uploading, there does not seem to be any functionality to download data again. So it's usefulness is limited to rendering and cannot be used for general purpose simulations of the ECS (which I was kinda hoping to see :( ).

But that data can still be used in compute shaders to prepare for rendering. Things like computing the model-to-screenspace-matrix, entire object culling by frustum, culling by (reprojected) hierarchical depth buffer, determining the LOD to render and then writing all passing objects into a buffer for a following "instanced indirect draw" to read them (as well as configuring the amount of instances for the draw which may be 0 too). In fact this is pretty standard practice for larger engines which need to render lots of things, as culling entire objects is a lot cheaper than culling individual triangles. You'll find plenty of articles about it even back from PS3 era.
You can also see these Compute Shaders in the repo:
https://github.com/AmbientRun/Ambient/blob/main/crates/renderer/src/collect.wgsl
https://github.com/AmbientRun/Ambient/blob/main/crates/renderer/src/culling.wgsl
