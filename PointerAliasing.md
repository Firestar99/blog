> One of Rust's axioms (immutable rules by which all code, safe Rust or unsafe Rust or C code being called on Rust stuff through an FFI etc) is that data can not be concurrently modified. All data, no matter what, can't be changed or written to by two threads at the same time.

This problem isn't actually stemming from using multiple threads, far from it: `pointer aliasing` is the actual problem.

Take this example in C/C++:
```Cpp
int foo(int *a, int *b) {
    *b = *a + 1;
    return *a;
}
```
Let's say you are an optimizer and optimize this function to:
```Cpp
int foo(int *a, int *b) {
    int load_a = *a;
    int store_b = load_a + 1;
    *b = store_b;
    return load_a;
}
```
Now double check my optimization and tell me whether this optimization is fine or not before continuing to read.



||Under normal circumstances this optimization is fine, let's have this main code and both variants will work:||
```Cpp
int a = 6;
int b = 42;
int result = foo(&a, &b);
assert(a == 6);
assert(b == 7);
assert(result == 6);
```
However this main code is also completely legal in C/Cpp:
```Cpp
int oops = 6;
int result = foo(&oops, &oops);
assert(oops == 6);
#if normal
assert(result == 7);
#elif optimized
assert(result == 6); // oh no!
#end
```
Calling the same function but with the two pointers **aliasing** each other (aka. pointing to the same int) breaks our optimization!
That is the aliasing problem. And it has nothing to do with using multiple threads, just with compiler optimizations.



To fix our optimization we would need to introduce another load of `*a` in case we get aliased pointers:
```Cpp
int foo(int *a, int *b) {
    int load_a = *a;
    int store_b = load_a + 1;
    *b = store_b;
    int load_a_again = *a;
    return load_a_again;
}
```
You can also opt into **not** allowing these two pointers to alias to allow for greater optimisations by the compiler, but then **you have to guarantee yourself** that any pointer passed do not alias or you get UB (Undefined Behaviour). In C you can add the `restrict` keyword, in C++ there is no standard :cry: but most compilers (Clang, GCC) will accept `__restrict`:
```Cpp
int foo(int __restrict *a, int __restrict *b) {
    *b = *a + 1;
    return *a;
}
```



Now Rust has a very unique way of solving the problem, have a go to write this same function yourself in Rust!
||And you will quickly notice that due to modifiable references `&mut` having exclusive access to the thing they are referencing, you cannot ever create two mutable references to the same thing to be able to pass into this function. And thus there is no pointer aliasing with rust references! (unsafe rust: raw pointers may still alias and thus may not optimise as well)||
```rust
fn foo(a: &mut i32, b: &mut i32) -> i32 {
    *b = a + 1;
    a
}

let oops: i32 = 6;
let result = foo(&mut oops, &mut oops);
//                          ^^^^ compiler error: no two mulable references to the same object!
```

If you really do need both to be mutable, you'd have to pass a reference to a `Cell`/`RefCell`/`UnsafeCell` and use their specific methods, feel free to research yourself how they all individually handle this situation
