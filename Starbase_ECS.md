# Starbase ECS

Disclaimer: This is just how I remember it, and none of code shown is actually from the used system, and any mentioned details can be incorrect.

Starbase has a completely custom ECS written for it's rather unique use-case, focused on efficiency and scalability rather than ease-of-use. Thus, it's probably not the best for any typical generic game which needs to support lots of use-cases.

First please google "starbase ships" and you'll see the general composition of the game: (usually) few entities but each entity has lots of components. Though components in this context is not the typical component of an ECS, but rather the building blocks out of which the ships build. They usually contain a beam structure covered by variously sized plates which are bolted onto the beams and inside the ship there are also more technical components like reactors, thrusters, cooling, controls, programming chips etc. So a typical ship is composed of 1 big Entity (if there are no hinges etc.) and potentially thousands of components.

## Entities

```c
struct EntityHandle {
    uint32_t handle;
};

struct EntityId {
    uint32_t id;
};

struct Entity {
    EntityId entityId;
    EntityTypeHandle type;
    EntityFlags flags;
    
    Vec3 position;
    Quaternion rotation;
    Vec3 velocity;
    Vec3 angular_velocity;
    
    CacheRangeVector<ComponentHandle, 1> components;
    optional<uniqueptr<ComponentOctree>> componentOctree;
    
    // other pointers / handles to eg. physics mesh, model transform, etc.
}

static IndexMap<EntityHandle, Entity> entities;
static HashMap<EntityId, EntityHandle> entityIdToHandle;
```

This is basically how the Entity is implemented and all further things will be. At the bottom there's an IndexMap which is basically a giant array of all the Entities. New Entities are allocated from the beginning of the array, empty spots filled first, and an EntityHandle is then created from the allocated index. And thus any access via an EntityHandle is an O(1) operation, just reading at the index of the array. I'm unsure if it's actually just a giant array or if it can scale up the amount of entries by maybe having multiple array "blocks" and allocating more. Additionally, there is an EntityId which serves as the unique identifier for each entity which may be stored on disk or transferred though network. As it has to be unique directly indexing into the array isn't possible, and instead it first has to translate the EntityId using the HashMap `entityIdToHandle` to an EntityHandle before being able to look up whatever properties, which is of course a bit more expensive than O(1) of Handles.

Back to the `struct Entity` it will of course store it's EntityId and also an EntityTypeHandle which works exactly the same way EntityHandle does, just this time it's for different types of Entities which does not need to be stored for each individual entity to save on memory. Then some frags for various optimizations and typical position rotation variables for it. The most important thing though is its RangeVector of ComponentHandles which stores the handles of all contained components, but not individually in a List but as a List of Ranges in which they are contained. When these entities get loaded they usually allocate thousands of Components all at once so that usually results in just one giant range making this very efficient, and add a Caching system that reserves enough space in the Struct for one Range entry similarly to SmallVec in Rust. Additionally, all the components are put into an Octree for quick and efficient ray traversal for queries and the like.

## Components

```c
struct ComponentHandle {
    uint32_t handle;
}

struct ComponentId {
    uint32_t id;
}

struct Component {
    ComponentTypeHandle type;
    EntityHandle entity;
    
    Vec3 position;
    Quaternion rotation;
    // no velocity!
    
    uint32_t partBitmap;
    optional<unqiue_ptr<VoxelDamage>> voxelDamage;
    
    ExtraComponentsDataHandle extraData;
    // [..]
}

static IndexMap<ComponentHandle, Component> components;
static HashMap<ComponentId, ComponentHandle> componentIdToHandle;
```
Going to Components the general setup is again quite the same as Entities, so we can jump straight to the struct which of course starts with the same type handle and an EntityHandle as a parent pointer. Next Components are always strongly attached to an Entity they only get a position and rotation, but no velocity. If you wanted to deattach some Component because you removed the bolt holding it on your ship, you will have to create a new Entity and move that Component to it, so it can move independently. Similarly, all hinges or doors are also separate Entities from your ship and are attached via constraints in the physics engine, which can sometimes make them bug out. 

But Components can also be damaged which in Starbase is split into two categories: Fracture Damage and Voxel Damage. Each Object during building has its volume split up into up to 32 sections computed using 3D voronoi, and when an object receives fracture damage a section may be removed by removing bits from `uint32_t partBitmap`. Voxel damage is tracked by creating a 3D Texture where individual voxels may be removed, but storing and networking that is expensive so this sort of damage is rarely used and effectively only reserved to player tools and weapons. 

As we are just talking about Rendering all components of a ship are being collected per type and drawn using instancing. If a component took damage that would need to be rendered using a separate draw call(s): If it took fracture damage each fracture would be rendered individually, if it took voxel damage then it would render in its entirety but with the voxel 3D Texture attached to raymarch though it in the fragment shader. If any change happened in the ECS a change entry would be added to a queue and sometime later the render thread would apply all these between frames to its own data structures. All of these components would also be culled on the GPU against a pre-depth image. Some components which the ships had lots of like Bolts actually got a separate system to render them more efficiently.

```c
// ComponentExtraData
struct ComponentExtraDataHandle {
    uint32_t handle;
}

struct ComponentExtraData {
    uint64_t someServerTicket;
    // [..]
}

static IndexMap<ComponentExtraDataHandle, ComponentExtraData> componentExtraData;
```
ComponentExtraData is optional for components and is lazy allocated. It typically contains gameplay related extra information on components only few components need, thus it's more efficient to move those out to a separately allocated struct to save on memory. There may also have been multiple ComponentExtraData for different use-cases, but I don't remember the actual details about it.

## Optimizations and Threading

```c
enum EntityFlags {
    DEFAULT,
    SINGLE_COMPONENT,
    // [...]
}
```

Talking a bit about optimization: In the game your typical Entity is either one of two extremes: Either a Ship-like thing with one Entity and thousands of Components, or a simple thing with many Entities each just having one Component. The latter for example could be a nugget mined out of an Asteroid and you can mine a lot of nuggets. So it makes sense to flag these single component entities using EntityFlags as they don't need an Octree for their Components, or a cached physics mesh as they can just directly use the physics mesh of that single component.

But as you may notice this entire ECS is not build to be accessed concurrently. You can only modify it from the main thread and threading was usually done by each system individually. Typical scenarios are to gather all the info needed on the main thread (like all the components types, positions and rotations of an entity), then submit an async task that would work on that (e.g. build a physics mesh) and then submit the result back to the main thread. This workflow is especially prevalent throughout the physics system, the damage calculation system, and Devices.

## Devices, complex Components

Devices are complex Components which do have extra logic attached to them, like a Generator, Thruster, Fuel tank, Cooling etc. I don't remember exactly how they were associated with their Component, I'd assume something along `HashMap<ComponentHandle, unique_ptr<Device>>` where the Device would be a base class to various Devices. They would also very much follow the "gather on main, process async" threading rule so that each Device could easily talk to each other to e.g. use electricity. The programmable Yolol chips would also execute in this ship thread as the interpreted language wouldn't be exactly fast to process and commonly needed to talk to whatever devices present on the ship.
